#coding=utf-8
import os
import xlrd
import sys
import chardet
import pandas
import csv
import copy
from datetime import date, datetime
from itertools import groupby


default_encoding = "utf-8"
if default_encoding != sys.getdefaultencoding():
    reload(sys)
    sys.setdefaultencoding(default_encoding)


def decode(content):
    try:
        if isinstance(content, (str, unicode)):
            content = content.decode("utf-8").encode("gbk")
        return content
    except Exception, e:
        print transfer_decode("字符%s出现异常，已用#代替" % content)
    return "#"


def data_decode(content):
    return content.decode(chardet.detect(content)['encoding'])


def encode_excel_data(data):
    try:
        return data.encode("utf-8")
    except Exception, e:
        return data


def transfer_decode(strs):
    return strs.decode(chardet.detect(strs)['encoding'])


def read_execl_data(filename, title=[]):
    data = xlrd.open_workbook(filename)
    sheets = data.sheets()
    all_data = []
    result = {}
    for sheet in sheets:
        nrows = sheet.nrows
        start_row = 0
        if nrows == 0:
            continue
        if len(title) == 0:
            title = range(0, len(sheet.row_values(0)))
            title_str = sheet.row_values(0)
            start_row = 1
        for i in range(start_row, nrows):
            row_content = sheet.row_values(i)
            for k in range(0, len(row_content)):
                """
                Empty
                """
                if sheet.cell(i, k).ctype == 0:
                    row_content[k] = "None"
                '''
                Date
                '''
                if sheet.cell(i, k).ctype == 3:
                    date_value = xlrd.xldate_as_tuple(sheet.cell_value(i, k), data.datemode)
                    row_content[k] = date(*date_value[:3]).strftime('%Y-%m-%d')
                if sheet.cell(i, k).ctype == 5:
                    row_content[k] = "*ERROR*"
            all_data.append(row_content)

    for i, field in enumerate(title):
        field_dict = dict()
        field_dict[field] = []
        for row in all_data:
            if len(row) < i:
                field_dict[field].append("None")
            else:
                field_dict[field].append(row[i])
        result.update(field_dict)
    return title_str, result


def handle_index_data(index, data):
    return


def save_file(content, filename, title=[]):
    # title = ["证券代码",
    #          "证券简称",
    #          "公司中文名",
    #          "上市地点",
    #          "省份",
    #          "市盈率PE",
    #          "票面利率(发行时)",
    #          "上市日期"
    #          ]
    csvfile = file(filename, 'wb')
    writer = csv.writer(csvfile)
    if len(title) > 0:
        title = map(decode, title)
        writer.writerow(title)
    for each in content:
        row = map(decode, each)

        writer.writerow(row)
    csvfile.close()


def create_title(title_a, title_b, title_list):
    tmp_list = copy.copy(title_list)
    for i, title in enumerate(title_list):
        if "key" in str(title):
            continue
        if title in range(0, len(title_a)):
            tmp_list[i] = title_a[title]
            continue
        if title in range(0, len(title_b)):
            tmp_list[i] = title_b[title]
            continue
        if "x" in title:
            index = int(title.split("_")[0])
            if index is "key":
                index = int(title.split("_")[1])
            tmp_list[i] = title_a[index]
        if "y" in title:
            index = int(title.split("_")[0])
            if index is "key":
                index = int(title.split("_")[1])
            tmp_list[i] = title_b[index]
    result = []
    for i in tmp_list:
        if "key" not in i:
            result.append(i)
    return result


if __name__ == "__main__":
    dataframe1 = pandas.DataFrame(read_execl_data(decode("公司债.xlsx"))[1])
    title1, data = read_execl_data(decode("公司债.xlsx"))
    title2, data = read_execl_data(decode("上市公司.xlsx"))
    dataframe2 = pandas.DataFrame(read_execl_data(decode("上市公司.xlsx"))[1])
    # save_file(pandas.merge(dataframe1, dataframe2, left_on=2, right_on=3).values, "test.csv")
    create_title(title1, title2, list(pandas.merge(dataframe1, dataframe2, on=2)))