#coding=utf-8
from file_help import read_execl_data, save_file, decode, create_title
import sys, traceback
import os
import pandas
import json
default_encoding = "utf-8"
if default_encoding != sys.getdefaultencoding():
    reload(sys)
    sys.setdefaultencoding(default_encoding)


FILEANAME = "FileA"
FILEBNAME = "FileB"
CONFIGDEFAULTSTR = "DEFAULT"
JOINFILENAME = "合并文件.csv".decode("utf-8")
DIFFERENTFILENAME = "差集文件.csv".decode("utf-8")


def get_config():
    configuration_path = os.path.join(os.path.dirname(__file__), "configuration")
    with open(configuration_path, "r") as f:
        config = f.read()
    config = json.loads(config)
    return config


def load_default_configuration():
    configs = get_config()
    if configs["FileA_path"] == CONFIGDEFAULTSTR:
        configs["FileA_path"] = os.path.join(os.path.dirname(__file__), FILEANAME)
    if configs["FileB_path"] == CONFIGDEFAULTSTR:
        configs["FileB_path"] = os.path.join(os.path.dirname(__file__), FILEBNAME)
    if configs["Results_path"] == CONFIGDEFAULTSTR:
        configs["Results_path"] = os.path.join(os.path.dirname(__file__), "Result")

    if os.path.exists(configs["Results_path"]) is False:
        os.mkdir(configs["Results_path"])

    if os.path.exists(configs["FileA_path"]) is False:
        os.mkdir(configs["FileA_path"])

    if os.path.exists(configs["FileB_path"]) is False:
        os.mkdir(configs["FileB_path"])

    return configs


def get_all_content_from_path(path):
    all_content = {}
    for each_file in os.listdir(path):
        if "xlsx" not in each_file or "xls" not in each_file:
            # print transfer_decode("忽略了不规则文件 %s" % each_file)
            continue
        file_full_name = os.path.join(path, each_file)
        title, content = read_execl_data(file_full_name)
        for key in content.keys():
            if key in all_content.keys():
                all_content[key].extend(content[key])
            else:
                all_content[key] = content[key]

    return title, all_content


def transfer_decode(strs):
    return strs.decode("utf-8")


def main():
    print transfer_decode("载入默认配置")
    config = load_default_configuration()
    print transfer_decode("读取A文件夹下的所有文件")
    file_a_title, file_a_content = get_all_content_from_path(config['FileA_path'])
    print transfer_decode("读取B文件夹下的所有文件")
    file_b_title, file_b_content = get_all_content_from_path(config['FileB_path'])
    print transfer_decode("载入pandas数据集合")
    data_frame_a = pandas.DataFrame(file_a_content)
    data_frame_b = pandas.DataFrame(file_b_content)
    if data_frame_a.size == 0:
        print transfer_decode("FileA中无有效数据，程序结束")
        return
    if data_frame_b.size == 0:
        print transfer_decode("FileB中无有效数据，程序结束")
        return
    print transfer_decode("开始计算合并数据")
    join_result_frame = pandas.merge(data_frame_a,
                                     data_frame_b,
                                     left_on=config['FileA_join_index'],
                                     right_on=config['FileB_join_index'])
    print transfer_decode("开始计算差集数据")
    different_result_frame = pandas.merge(data_frame_a,
                                          data_frame_b,
                                          left_on=config['FileA_join_index'],
                                          right_on=config['FileB_join_index'],
                                          how=config['Different'])

    print transfer_decode("正在生成标题")
    join_title = create_title(file_a_title, file_b_title, list(join_result_frame))
    different_title = create_title(file_a_title, file_b_title, list(different_result_frame))
    print transfer_decode("正在生成结果文件")
    join_file_path = os.path.join(config['Results_path'], JOINFILENAME)
    diff_file_path = os.path.join(config['Results_path'], DIFFERENTFILENAME)
    save_file(join_result_frame.values, join_file_path, join_title)
    save_file(different_result_frame.values, diff_file_path, different_title)
    print transfer_decode("完事收工")

if __name__ == "__main__":
    # main()
    try:
        print transfer_decode("*****************************程序开始*********************************")
        main()
        print transfer_decode("*****************************程序结束*********************************")
    except Exception, e:
        print transfer_decode("*****************************错误结束*********************************")
        print str(e)
        print traceback.print_stack()
        print ""
        print ""
        print ""
        print transfer_decode("出BUG啦，请速速联系小哥")
    print ""
    print transfer_decode("按 Enter 键结束程序".decode())
    name = raw_input()
